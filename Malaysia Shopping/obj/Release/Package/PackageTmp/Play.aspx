﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Play.aspx.cs" Inherits="Malaysia_Shopping.Play" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
    <script type="text/javascript">
        $(document).ready(function () {
            setPage('play');
        })
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <asp:HiddenField ID="FacebookID" runat="server" ClientIDMode="Static" />
    <asp:HiddenField ID="FacebookName" runat="server" ClientIDMode="Static" />
    <asp:HiddenField ID="SignedRequest" runat="server" ClientIDMode="Static" />

    <div class="guideline">
        Click vào một món đồ bất kỳ để có cơ hội nhận phần quà may mắn
    </div>
    <asp:Button runat="server" ID="Good01" CssClass="goods good01" OnClick="Good_Click" />
    <asp:Button runat="server" ID="Good02" CssClass="goods good02" OnClick="Good_Click" />
    <asp:Button runat="server" ID="Good03" CssClass="goods good03" OnClick="Good_Click" />
    <div class="guide">
        Click vào một
                <br />
        món đồ bất kỳ
    </div>
    <asp:MultiView runat="server" ID="Views" ActiveViewIndex="-1">
        <asp:View runat="server" ID="PlayedView">
            <script type="text/javascript">
                $(document).ready(function () {
                    showPopup('', 504, 271, '', 'play-popup');
                });
            </script>
            <div id="play-popup" class="popup-bg">
                <div class="title">
                    <span></span>
                    <a class="close" href="javascript:void(0);" onclick="hidePopup('play-popup');"></a>
                </div>
                <div class="body">
                    <h2>Bạn đã tham gia lượt chơi ngày hôm nay rồi.</h2>
                    <br />
                    <div style="font-size: 12px;">Hãy ghé lại và thử vận may của bạn vào ngày mai nhé!</div>
                    <br />
                    <br />
                    Click để biết thêm <strong>thông tin mua sắm</strong> và <strong>du lịch</strong>
                    <br />
                    nhân dịp khuyến mãi cuối năm tại Malaysia.
                    <br />
                    <a class="button" style="margin-left: 98px; margin-top: 10px;" href="https://www.facebook.com/tourismmalaysiavietnam/app_519242214767353" target="_blank">Các gói tour</a>
                    <a class="button" style="margin-left: 10px; margin-top: 10px; width: 160px;" href="https://www.facebook.com/tourismmalaysiavietnam/app_571497422866897" target="_blank">Trung tâm mua sắm</a>
                    <br class="clear" />
                    <br />
                    <br />
                    Nếu bạn đã từng đến Malaysia, <strong>hãy tham gia chia sẻ ngay</strong>
                    <br />
                    để có cơ hội đến thăm đất nước này lần nữa
                    <br />
                    <a class='button' style='margin-left: 183px; margin-top: 10px;' href="https://www.facebook.com/tourismmalaysiavietnam/app_493061797404763" target="_blank">Chia sẻ</a>
                </div>
            </div>
        </asp:View>
        <asp:View runat="server" ID="FailView">
            <script type="text/javascript">
                $(document).ready(function () {
                    showPopup('', 504, 271, '', 'play-popup');
                });
            </script>
            <div id="play-popup" class="popup-bg">
                <div class="title">
                    <span></span>
                    <a class="close" href="javascript:void(0);" onclick="hidePopup('play-popup');"></a>
                </div>
                <div class="body">
                    <h2>Rất tiếc, hôm nay bạn chưa trúng quà may mắn.</h2>
                    <br />
                    <div style="font-size: 12px;">Hãy ghé lại và thử vận may của bạn vào ngày mai nhé!</div>
                    <br />
                    <br />
                    Click để biết thêm <strong>thông tin mua sắm</strong> và <strong>du lịch</strong>
                    <br />
                    nhân dịp khuyến mãi cuối năm tại Malaysia.
                    <br />
                    <a class="button" style="margin-left: 98px; margin-top: 10px;" href="https://www.facebook.com/tourismmalaysiavietnam/app_519242214767353" target="_blank">Các gói tour</a>
                    <a class="button" style="margin-left: 10px; margin-top: 10px; width: 160px;" href="https://www.facebook.com/tourismmalaysiavietnam/app_571497422866897" target="_blank">Trung tâm mua sắm</a>
                    <br class="clear" />
                    <br />
                    <br />
                    Nếu bạn đã từng đến Malaysia, <strong>hãy tham gia chia sẻ ngay</strong>
                    <br />
                    để có cơ hội đến thăm đất nước này lần nữa
                    <br />
                    <asp:Literal runat="server" ID="FailShareLink"></asp:Literal>
                    <a class='button' style='margin-left: 183px; margin-top: 10px;' href="https://www.facebook.com/tourismmalaysiavietnam/app_493061797404763" target="_blank">Chia sẻ</a>
                </div>
            </div>
        </asp:View>
        <asp:View runat="server" ID="WinView">
            <script src="Scripts/jquery.validate.min.js" type="text/javascript"></script>
            <script type="text/javascript">
                $(document).ready(function () {
                    showPopup('', 476, 513, '', 'play-popup');

                    // Validate inputs
                    $('form').validate({
                        errorLabelContainer: $("form div.error"),
                        onkeyup: false,
                        onfocusout: false,
                        invalidHandler: function (form, validator) {
                            $('form div.error label:not(.error)').remove();
                        }
                    });
                    $('#Fullname').rules("add", {
                        required: true,
                        messages: {
                            required: "Bạn chưa nhập Họ và Tên"
                        }
                    });
                    $('#Email').rules("add", {
                        required: true,
                        email: true,
                        messages: {
                            required: "Bạn chưa nhập Địa chỉ email",
                            email: "Địa chỉ email không hợp lệ"
                        }
                    });
                    $('#PhoneNumber').rules("add", {
                        required: true,
                        number: true,
                        minlength: 6,
                        maxlength: 15,
                        messages: {
                            required: "Bạn chưa nhập Số điện thoại",
                            number: "Số điện thoại không hợp lệ",
                            minlength: "Số điện thoại không hợp lệ",
                            maxlength: "Số điện thoại không hợp lệ"
                        }
                    });
                    $('#Address').rules("add", {
                        required: true,
                        messages: {
                            required: "Bạn chưa nhập Địa chỉ"
                        }
                    });
                });
            </script>
            <div id="play-popup" class="popup-bg">
                <div class="title">
                    <span></span>
                    <a class="close" href="javascript:void(0);" onclick="hidePopup('play-popup');"></a>
                </div>
                <div class="body">
                    <h2 style="color: #a71e97; font-size: 20px;">chúc mừng bạn
                        <br />
                        đã trúng một phần quà may mắn
                    </h2>
                    <br />
                    <br />
                    <div style="text-transform: uppercase">
                        <asp:Literal runat="server" ID="GiftName"></asp:Literal>
                    </div>
                    <br />
                    <asp:Image runat="server" ID="GiftImage" Height="170" />
                    <div style="text-transform: uppercase">
                        Vui lòng nhập thông tin của bạn
                    </div>
                    <div class="error">
                        <asp:Literal runat="server" ID="ErrorMessage"></asp:Literal>
                    </div>
                    <div class="textbox-container">
                        <asp:TextBox runat="server" ID="Fullname" CssClass="textbox" ClientIDMode="Static" placeholder="Họ và tên"></asp:TextBox>
                        <asp:Label runat="server" AssociatedControlID="Fullname" CssClass="name"></asp:Label>
                    </div>
                    <div class="textbox-container">
                        <asp:TextBox runat="server" ID="Email" CssClass="textbox" ClientIDMode="Static" placeholder="Email"></asp:TextBox>
                        <asp:Label runat="server" AssociatedControlID="Email" CssClass="email"></asp:Label>
                    </div>
                    <div class="textbox-container">
                        <asp:TextBox runat="server" ID="PhoneNumber" CssClass="textbox" ClientIDMode="Static" placeholder="Số điện thoại"></asp:TextBox>
                        <asp:Label runat="server" AssociatedControlID="PhoneNumber" CssClass="phone"></asp:Label>
                    </div>
                    <div class="textbox-container">
                        <asp:TextBox runat="server" ID="Address" CssClass="textbox" ClientIDMode="Static" placeholder="Địa chỉ"></asp:TextBox>
                        <asp:Label runat="server" AssociatedControlID="Address" CssClass="email"></asp:Label>
                    </div>
                    <div class="textbox-container">
                        <asp:Button runat="server" ID="Submit" CssClass="button" Text="Hoàn tất" Width="100" Style="margin-left: 188px; margin-top: 10px;" OnClick="Submit_Click" />
                    </div>
                    <div style="position: absolute; bottom: 10px; left: 165px; font-size: 11px;">(Hình ảnh trên chỉ mang tính chất minh họa)</div>
                </div>
            </div>
        </asp:View>
        <asp:View runat="server" ID="ShareView">
            <script type="text/javascript">
                $(document).ready(function () {
                    showPopup('', 504, 201, '', 'play-popup');
                });
            </script>
            <div id="play-popup" class="popup-bg">
                <div class="title">
                    <span></span>
                    <a class="close" href="javascript:void(0);" onclick="hidePopup('play-popup');"></a>
                </div>
                <div class="body">
                    Click để biết thêm <strong>thông tin mua sắm</strong> và <strong>du lịch</strong>
                    <br />
                    nhân dịp khuyến mãi cuối năm tại Malaysia.
                    <br />
                    <a class="button" style="margin-left: 98px; margin-top: 10px;" href="https://www.facebook.com/tourismmalaysiavietnam/app_519242214767353" target="_blank">Các gói tour</a>
                    <a class="button" style="margin-left: 10px; margin-top: 10px; width: 160px;" href="https://www.facebook.com/tourismmalaysiavietnam/app_571497422866897" target="_blank">Trung tâm mua sắm</a>
                    <br class="clear" />
                    <br />
                    <br />
                    Nếu bạn đã từng đến Malaysia, <strong>hãy tham gia chia sẻ ngay</strong>
                    <br />
                    để có cơ hội đến thăm đất nước này lần nữa
                    <br />
                    <asp:Literal runat="server" ID="ShareLink"></asp:Literal>
                    <a class='button' style='margin-left: 183px; margin-top: 10px;' href="https://www.facebook.com/tourismmalaysiavietnam/app_493061797404763" target="_blank">Chia sẻ</a>
                </div>
            </div>
        </asp:View>
    </asp:MultiView>
    <div class="loading">
        <table style="width: 100%; height: 100%;">
            <tr>
                <td style="text-align: center; vertical-align: middle;">
                    <img src="Images/loading.gif" alt="Loading" />
                </td>
            </tr>
        </table>
    </div>
</asp:Content>
