﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Popup.Master" AutoEventWireup="true" CodeBehind="CentralMarket.aspx.cs" Inherits="Malaysia_Shopping.CentralMarket" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <style type="text/css">
        html, body {
            padding: 20px 10px 40px 20px;
        }

        .content {
            width: 526px;
            height: 500px;
            padding-right: 30px;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="content">
        <table cellpadding="0" cellspacing="0">
            <thead>
                <tr>
                    <td>Ngày</td>
                    <td>Sự kiện/ Khuyến mãi</td>
                    <td>Chi tiết</td>
                </tr>
            </thead>
            <tr>
                <td>8-9/12</td>
                <td>Year End Sale & School Holiday Carnival</td>
                <td>Những hoạt động đặc sắc bao gồm cuộc thi thiết kế túi mua sắm, hoạt động gia đình, rút thăm trúng thưởng.</td>
            </tr>
        </table>
    </div>
</asp:Content>
