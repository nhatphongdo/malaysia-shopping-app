﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Popup.Master" AutoEventWireup="true" CodeBehind="Term.aspx.cs" Inherits="Malaysia_Shopping.Term" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <style type="text/css">
        html, body {
            padding: 20px 10px 40px 20px;
        }

        .content {
            width: 526px;
            height: 500px;
            padding-right: 30px;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <h2 class="title"></h2>
    <br />
    <div class="content">
        <strong>1. Thời gian: Từ 10/12/2012 đến 04/01/2013.</strong>
        <br />
        <strong>2. Đối tượng tham gia:</strong>
        <p>
            Tất cả các bạn nam, nữ là công dân Việt Nam, không giới hạn độ tuổi, vùng miền.
        </p>
        <strong>3. Cơ cấu giải thưởng:</strong>
        <ul>
            <li>Giải nhất: 5 máy chụp hình Fujifilm Instax mini</li>
            <li>Giải nhì: 40 túi du lịch mini</li>
            <li>Giải ba: 70 bao đựng passport</li>
            <li>Giải khuyến khích: 150 phần quà nhỏ</li>
        </ul>
        <strong>4. Hình thức tham gia:</strong>
        <ul>
            <li><u>Bước 1:</u>
                <br />
                Like fanpage Tourism Malaysia Việt Nam và đọc thể lệ để tham gia cuộc thi.
            </li>
            <li><u>Bước 2:</u>
                <br />
                Click chọn bất kỳ món hàng nào mà bạn thích trong cửa hàng. Nếu món hàng bạn chọn là một món hàng may mắn, bạn sẽ nhận được một phần quà từ Tourism Malaysia Vietnam.
                <br />
                Lưu ý: Mỗi ngày, người chơi chỉ có duy nhất 1 cơ hội trúng quà may mắn.
            </li>
            <li><u>Bước 3:</u>
                <br />
                Điền đầy đủ thông tin để BTC liên lạc sau khi bạn trúng quà.
                <br />
                Lưu ý: BTC không chịu trách nhiệm nếu bạn không điền đầy đủ thông tin.
            </li>
        </ul>
        <strong>5. Cách thức nhận giải</strong>
        <ul>
            <li>Tất cả người tham dự cuộc thi phải chấp nhận những quy định do BTC đưa ra.</li>
            <li>Người thắng giải sẽ được thông báo qua e-mail, trên Tourism Malaysia Vietnam Fan page và điện thoại muộn nhất là sau 1 ngày kể từ ngày thông báo kết quả.</li>
            <li>Đối với người thắng giải ở TP.HCM, bạn mang CMND đến nhận giải thưởng trực tiếp tại đơn vị tổ chức. Đối với các trường hợp người thân đến nhận thay, cần có giấy uỷ quyền kèm CMND của người trúng giải.</li>
            <li>Đối với người thắng giải không ở TP.HCM, BTC sẽ gửi quà cho bạn qua đường bưu điện.</li>
        </ul>
        <strong>6. Ban Tổ Chức:</strong>
        <ul>
            <li>Đơn vị tổ chức: Công ty cổ phần truyền thông tiếp thị Mekong – Đại diện truyền thông cho Tourism Malaysia Vietnam.</li>
            <li>Đơn vị tài trợ cuộc thi: Tổng Cục Du Lịch Malaysia tại Việt Nam.</li>
        </ul>
        <strong>7. Quy định chung:</strong>
        <ul>
            <li>Tất cả thông tin đăng ký tính xác thực và người đăng ký thông tin phải chịu hoàn toàn trách nhiệm trong trường hợp có khiếu nại về thông tin đăng ký.</li>
            <li>BTC có quyền sử dụng hình ảnh và thông tin của người chiến thắng để phục vụ cho mục đích quảng cáo thương mại mà không cần phải báo trước và trả bất cứ khoản phí nào.</li>
            <li>BTC không chịu trách nhiệm về:
                <ul>
                    <li>Các đăng ký chậm trễ, thất lạc, sai sót, chưa hoàn tất xảy ra trong lúc đăng ký do lỗi fan page, sơ suất, thay đổi, xoá bỏ, sai thông tin, gián đoạn đường truyền, virus, xâm nhập dữ liệu bất hợp pháp, dữ liệu hư hỏng, lạm dụng thông tin, hư hại phần cứng, phần mềm và các lý do khác khiến việc đăng ký không thành công.</li>
                    <li>Bất cứ điều kiện tạo ra bởi chương trình bị vượt tầm kiểm soát của nhà quản lý, điều đó có thể làm cuộc thi bị ngắt quãng và hủy bỏ;</li>
                </ul>
            </li>
            <li>BTC có thẩm quyền quyết định, hoãn lại hay hủy bỏ cuộc thi ở bất kể thời điểm nào nếu máy tính bị nhiễm virus, bị lỗi hay các vấn đề kỹ thuật khác làm gián đoạn sự quản lý, bảo mật. BTC cũng có quyền thay đổi nội dung cuộc thi, hay điều chỉnh thể lệ vào bất kì thời điểm nào trước khi kết thúc cuộc thi và sẽ đăng tải thể lệ đã chỉnh sửa trên fan page (nếu có).Trước khi đình chỉ hay hủy bỏ, người tham dự cam kết không tạo ra các nguyên nhân làm gián đoạn tới cuộc thi hay ngăn chặn người khác tham gia vào cuộc thi.</li>
            <li>Quyết định của BTC là quyết định cuối cùng. Mọi tranh chấp, khiếu nại, thắc mắc về quyết định của BTC đều không có giá trị.</li>
        </ul>
    </div>
</asp:Content>
