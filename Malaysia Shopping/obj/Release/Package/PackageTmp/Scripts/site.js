﻿function setPage(css) {
    $('.page').addClass(css);
}

function showPopup(title, width, height, bodyUrl, popupid) {
    var selector = '.popup-bg ';
    if (popupid) {
        selector = '#' + popupid + ' ';
    }
    $(selector + '.title span').text(title);
    $(selector + '.body').width(width);
    $(selector + '.body').height(height);
    $(selector + '.title').width($(selector + '.body').outerWidth());
    $(selector).fadeIn();
    if (bodyUrl != "") {
        $(selector + '.body').html("");
        $(selector + '.body').append("<iframe src='" + bodyUrl + "' scrolling='no'></iframe>");
    }
}

function hidePopup(popupid) {
    if (popupid) {
        $('#' + popupid).fadeOut();
    }
    else {
        $('.popup-bg').fadeOut();
    }
}