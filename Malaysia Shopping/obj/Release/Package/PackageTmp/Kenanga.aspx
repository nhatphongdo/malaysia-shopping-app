﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Popup.Master" AutoEventWireup="true" CodeBehind="Kenanga.aspx.cs" Inherits="Malaysia_Shopping.Kenanga" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <style type="text/css">
        html, body {
            padding: 20px 10px 40px 20px;
        }

        .content {
            width: 526px;
            height: 500px;
            padding-right: 30px;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="content">
        <table cellpadding="0" cellspacing="0">
            <thead>
                <tr>
                    <td>Ngày</td>
                    <td>Sự kiện/ Khuyến mãi</td>
                    <td>Chi tiết</td>
                </tr>
            </thead>
            <tr>
                <td>1/11-31/12</td>
                <td>Enjoy the best Offers & Plans with MYES KWC DiGi rewards!</td>
                <td>Tham gia hoạt động tại cửa hàng Flash Digi để tận dụng những cơ hội tuyệt vời! Với mỗi biên lai trị giá 150 Ringgit, khách hàng  sẽ sở hữu một chiếc ô miễn phí.</td>
            </tr>
            <tr>
                <td></td>
                <td>KWC Fashion Christmas Celebration</td>
                <td>Ăn mừng Giáng sinh thời thượng, xem biểu diễn thời trang KWC Xmas với các phong cách mới nhất cho mùa Giáng sinh.</td>
            </tr>
            <tr>
                <td></td>
                <td>KWC Gift Redemption </td>
                <td>Với hóa đơn 100 Ringgit, quý khách sẽ có cơ hội rút thăm quà Giáng sinh đặc biệt.</td>
            </tr>
            <tr>
                <td>2/12</td>
                <td>‘Fashionista!’
                    <br />
                    KWC MYES Treasure Hunt
                </td>
                <td>Hoặc tham gia cuộc truy lùng thách thức sáng tạo, giác quan thời trang, sự nhanh nhẹn và trí óc trong thời gian giới hạn và ngân sách hạn chế để giành chiến thắng trong giải thưởng cao cấp.</td>
            </tr>
        </table>
    </div>
</asp:Content>
