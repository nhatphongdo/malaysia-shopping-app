﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Popup.Master" AutoEventWireup="true" CodeBehind="Suria.aspx.cs" Inherits="Malaysia_Shopping.Suria" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <style type="text/css">
        html, body {
            padding: 20px 10px 40px 20px;
        }

        .content {
            width: 526px;
            height: 500px;
            padding-right: 30px;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="content">
        <table cellpadding="0" cellspacing="0">
            <thead>
                <tr>
                    <td>Ngày</td>
                    <td>Sự kiện/ Khuyến mãi</td>
                    <td>Chi tiết</td>
                </tr>
            </thead>
            <tr>
                <td>30/11-29/12</td>
                <td>Christmas Campaign</td>
                <td>Với mỗi hóa đơn từ 500 Ringgit trở lên (gộp tối đa 2 biên lai) để nhận một găng tay Giáng sinh.
                    <br />
                    <br />
                    Với mỗi hóa đơn từ 3000  Ringgit trở lên  (gộp tối đa 2 biên lai) để nhận từ Melvita quà tặng Giáng sinh đặc biệt  (Chỉ áp dụng vào thứ Bảy &amp; Chủ Nhật, giới hạn tối đa 30 khách hàng mỗi ngày).
                </td>
            </tr>
        </table>
    </div>
</asp:Content>
