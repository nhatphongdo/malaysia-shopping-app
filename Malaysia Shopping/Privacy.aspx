﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Popup.Master" AutoEventWireup="true" CodeBehind="Privacy.aspx.cs" Inherits="Malaysia_Shopping.Privacy" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <style type="text/css">
        html, body {
            padding: 20px 10px 40px 20px;
        }

        .content {
            width: 526px;
            height: 450px;
            padding-right: 30px;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <h2 class="title">Vui lòng đọc kỹ chính sách bảo mật trước khi sử dụng hoặc cung cấp thông tin cho trang web này. </h2>
    <br />
    <div class="content">
        <strong>1. Sử dụng thông tin</strong>
        <p>Chúng tôi thu thập thông tin cá nhân, tuy nhiên khi bạn được yêu cầu cung cấp thông tin cá nhân cho các mục đích nhất định được nói rõ trong trang web thì bạn phải đảm bảo cung cấp các thông tin tôn trọng quyền cá nhân của những người sử dụng trang web này. Để truy cập vào trang web này, chúng tôi không yêu cầu phải cung cấp những trung thực và chính xác.</p>
        <p>Tất cả những thông tin cá nhận thu thập được qua trang web này sẽ được áp dụng đúng theo chính sách bảo mật này và Điều 38 của Bộ Luật Dân Sự Việt Nam được ban hành ngày 14 tháng 7 năm 2005 và có hiệu lực từ ngày 01 tháng 01 năm 2006.</p>
        <p>Tất cả những nhận xét, bình luận, đề xuất hoặc các thông tin ngoài dữ liệu cá nhân được gửi đến cho chúng tôi sẽ được xem là tự nguyện cung cấp cho Tourism Malaysia Việt Nam trên cơ sở không bảo mật và không có tính sở hữu.</p>
        <strong>2. Tiết lộ thông tin</strong>
        <p>Chúng tôi có thể tiết lộ những thông tin cá nhân mà bạn cung cấp bằng email hoặc các phương tiện khác cho đối tác đồng ý tuân theo những quy định trong chính sách bảo mật này của Tourism Malaysia Việt Nam và Tourism Malaysia trên toàn cầu. </p>
        <p>Trang web này có thể chứa đựng các liên kết hoặc hoặc tham chiếu đến một số trang web khác mà tại đó chính sách bảo mật này có thể không được áp dụng. Bạn nên chắc rằng mình đã đọc và đồng ý với chính sách bảo mật của  bất kỳ trang web nào mà bạn truy cập. </p>
        <p>Khi cung cấp thông tin cho trang web này, bạn mặc nhiên đã đồng ý với điều khoản sử dụng thông tin và tiết lộ thông tin quy định trong Chính sách bảo mật này. </p>
        <p>Tourism Malaysia Việt Nam có toàn quyền thay đổi hay cập nhật Chính sách Bảo mật này vào bất kỳ lúc nào. Khi có bất kỳ một sự thay đổi nào, chúng tôi sẽ đăng trên trang web này để bạn biết được nội dung và hình thức thay đổi đó. Mọi sự thay đổi hoặc bổ sung đó sẽ có giá trị ngay khi được đăng trên trang web này. Bạn vui lòng kiểm tra lại theo định kỳ, nhất là trước khi bạn cung cấp bất kỳ thông tin cá nhân nào.  </p>
        <p>Các điều kiện, điều khoản và nội dung của trang web này được xây dựng phù hợp với luật pháp Việt Nam.</p>
    </div>
</asp:Content>
