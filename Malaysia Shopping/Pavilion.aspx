﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Popup.Master" AutoEventWireup="true" CodeBehind="Pavilion.aspx.cs" Inherits="Malaysia_Shopping.Pavilion" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <style type="text/css">
        html, body {
            padding: 20px 10px 40px 20px;
        }

        .content {
            width: 526px;
            height: 500px;
            padding-right: 30px;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="content">
        <table cellpadding="0" cellspacing="0">
            <thead>
                <tr>
                    <td>Ngày</td>
                    <td>Sự kiện/ Khuyến mãi</td>
                    <td>Chi tiết</td>
                </tr>
            </thead>
            <tr>
                <td>15/11-1/1/13</td>
                <td>Fashion Avenue Dazzle</td>
                <td>Quà tặng trao ngay cho mỗi hóa đơn trị giá từ 600 Ringgit hoặc 500 Ringgit đối với  khác hàng thanh toán qua thẻ Citibank tại bất kỳ hệ thống cửa hàng nào của Fashion Avenue </td>
            </tr>
            <tr>
                <td></td>
                <td>Citibank Exclusives </td>
                <td>Khi thanh toán bằng thẻ Citibank tại bất kỳ cửa hàng Tiffany’s &amp; Co. nào, khách hàng sẽ có cơ hội trúng thưởng bộ tách đĩa và cặp khăn được thiết kế đặc biệt. Với hóa đơn trị giá từ 500 Ringgit, 8000 Ringgit và 25000 Ringgit, khách hàng sẽ có cơ hội được hoàn tiền. 
                </td>
            </tr>
            <tr>
                <td></td>
                <td>MasterCard Rewards</td>
                <td>Với mỗi hóa đơn thanh toán bằng thẻ MasterCard, khách hàng sẽ nhận ngay bộ sản phẩm dung cho nhà tắm của Crabtree &amp; Evelyn.
                </td>
            </tr>
        </table>
    </div>
</asp:Content>
