﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Popup.Master" AutoEventWireup="true" CodeBehind="Bangsar.aspx.cs" Inherits="Malaysia_Shopping.Bangsar" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <style type="text/css">
        html, body {
            padding: 20px 10px 40px 20px;
        }

        .content {
            width: 526px;
            height: 500px;
            padding-right: 30px;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="content">
        <table cellpadding="0" cellspacing="0">
            <thead>
                <tr>
                    <td>Ngày</td>
                    <td>Sự kiện/ Khuyến mãi</td>
                    <td>Chi tiết</td>
                </tr>
            </thead>
            <tr>
                <td>23/11– 30/12 </td>
                <td>Centre-wide redemption</td>
                <td>Quà tặng giỏ mua sắm dành cho khách hàng với hóa đơn từ 500 Ringgit trở lên </td>
            </tr>
            <tr>
                <td></td>
                <td>Weekend activities & performances
                    <br />
                    <br />
                    Christmas Weekend Bazaar
                </td>
                <td>Tại cửa hàng Santa &amp;Santarina&amp; sẽ diễn ra sự kiện 12 Ngày mừng Giáng sinh với buổi diễu hành của các nhân vật trong truyền thuyết, sự kiện làm thiệp dành cho trẻ em và còn nhiều hoạt động khác diễn ra từ 23/11 – 16/12/2012.</td>
            </tr>
        </table>
    </div>
</asp:Content>
