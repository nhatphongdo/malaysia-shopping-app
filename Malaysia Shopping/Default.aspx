﻿<%@ Page Title="Home Page" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Default.aspx.cs" Inherits="Malaysia_Shopping._Default" %>

<asp:Content runat="server" ID="FeaturedContent" ContentPlaceHolderID="HeadContent">
    <script type="text/javascript">
        $(document).ready(function () {
            setPage('home');
        })
    </script>
</asp:Content>
<asp:Content runat="server" ID="BodyContent" ContentPlaceHolderID="MainContent">
    <%--<a class="yes-logo"></a>--%>
    <a id="facebookLogin" href="javascript:void(0);" class="like-button"></a>
    <div class="prize-camera"></div>
    <div class="prize-bag"></div>
    <div class="prize-passportcover"></div>
    <div class="prize-other"></div>
    <div style="position: absolute; bottom: 145px; left: 65px; font-size: 11px; font-style: italic">(Hình ảnh chỉ mang tính chất minh họa)</div>
</asp:Content>
