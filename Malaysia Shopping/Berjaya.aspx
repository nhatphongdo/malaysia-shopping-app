﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Popup.Master" AutoEventWireup="true" CodeBehind="Berjaya.aspx.cs" Inherits="Malaysia_Shopping.Berjaya" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <style type="text/css">
        html, body {
            padding: 20px 10px 40px 20px;
        }

        .content {
            width: 526px;
            height: 500px;
            padding-right: 30px;
        }

        .auto-style1 {
            text-decoration: underline;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="content">
        <table cellpadding="0" cellspacing="0">
            <thead>
                <tr>
                    <td>Ngày</td>
                    <td>Sự kiện/ Khuyến mãi</td>
                    <td>Chi tiết</td>
                </tr>
            </thead>
            <tr>
                <td>24/11-26/12</td>
                <td>Campaign: Santa’s North Park</td>
                <td>Cùng bước trên Cầu thang Âm nhạc để khám phá công viên mùa đông của Ông già Nô-en tại Berjaya Times Square Kuala Lumpur.
                    <br />
                    <br />
                    <strong>Mua sắm hoàn tiền</strong>
                    <br />
                    <strong>1)	<span class="auto-style1">Sự kiện Luồng gió mê thuật</span></strong>
                    <br />
                    Khách hàng sẽ nhận được thẻ mát-xa tại Berjaya Times Square với mỗi hóa đơn mua sắm từ 250 Ringgit, tối đa 2 hóa đơn cùng ngày.
                    <br />
                    <br />
                    <strong>2) <span class="auto-style1">Sự kiện Đẹp nhất thế gian</span></strong>
                    <br />
                    Khách hàng sẽ được SUB International “tân trang miễn phí” khi mua sắm với hóa đơn từ 100 Rinngit, tối đa 2 hóa đơn cùng ngày.  
                    <br />
                    <br />
                    <strong>Cuộc thi <span class="auto-style1">Tỏa sáng và chiến thắng</span></strong>
                    <br />
                    Sau khi được “tân trang”, khách hàng có thể chụp hình tại quầy 1st Avenue và thử sức với cơ hội trúng giài là một khóa học trang điểm. 
                    <br />
                    Điều kiện và điều khoản áp dụng. Tham khảo website Facebook của cửa hàng để biết thêm chi tiết.
                </td>
            </tr>
            <tr>
                <td>10/11 – 1/1/13
                    <br />
                    <br />
                    <br />
                    <br />
                    24/11 & 1, 8, 15, 22, 24, 25 & 26/12
                </td>
                <td>Campaign: Cosmic Christmas Challenge</td>
                <td>Tham gia các trò chơi nhảy tại Công viên giải trí Berjaya Times Square.
                    <br />
                    <br />
                    <strong class="auto-style1">Thách thức từ vũ trụ</strong>
                    <br />
                    Hãy cùng kỷ niệm ngày nghỉ cùng nhau tại công viên với những trò chơi từ Oorts &amp; Ooops với sự quay trở lại của tín đồ Cosmo.
                    <br />
                    <br />
                    <strong class="auto-style1">Cuộc diễu hành của những người hát rong</strong>
                    <br />
                    Hãy cùng chiêm ngưỡng Ông già Nô-en, các chú tiểu yêu tinh và tiên Giáng sinh xuất hiện trên đường.
                    <br />
                    <br />
                    <strong class="auto-style1">Bước nhảy Giáng sinh</strong>
                    <br />
                    Cùng hòa nhịp vào các điệu nhảy Giáng sinh với những nghệ sĩ tài năng trong vô số trang phục lộng lẫy.
                </td>
            </tr>
        </table>
    </div>
</asp:Content>
