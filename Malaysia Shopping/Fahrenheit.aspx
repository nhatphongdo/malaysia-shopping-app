﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Popup.Master" AutoEventWireup="true" CodeBehind="Fahrenheit.aspx.cs" Inherits="Malaysia_Shopping.Fahrenheit" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <style type="text/css">
        html, body {
            padding: 20px 10px 40px 20px;
        }

        .content {
            width: 526px;
            height: 500px;
            padding-right: 30px;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="content">
        <table cellpadding="0" cellspacing="0">
            <thead>
                <tr>
                    <td>Ngày</td>
                    <td>Sự kiện/ Khuyến mãi</td>
                    <td>Chi tiết</td>
                </tr>
            </thead>
            <tr>
                <td>15/11-1/1/13</td>
                <td>“A Pop Up Doraemon Christmas” at fahrenheit88</td>
                <td>Chào mừng Giáng sinh với quà tặng và quà lưu niệm tại cửa hàng Doraemon nếu quý khách tìm thấy hình ảnh Doraemon trong các sản phẩm mua tại cửa hàng.</td>
            </tr>
            <tr>
                <td></td>
                <td>Festive Reward</td>
                <td>Quà tặng miễn phí cho khách hàng có hóa đơn 100 Ringgit tại bất kỳ cửa hàng nào hoặc với hóa đơn 300 Ringgit tại UNIQLO &amp; Outlet.</td>
            </tr>
            <tr>
                <td></td>
                <td>MasterCard Reward</td>
                <td>Quà tặng túi mỹ phẩm miễn phí khi khách hàng thanh toán 300 Ringgit thông qua MasterCard.</td>
            </tr>
            <tr>
                <td></td>
                <td>Free 2-Hour Parking</td>
                <td>Miễn phí 2 giờ đậu với mỗi hóa đơn chi tiêu 100 Ringgit tại bất kỳ cửa hàng nào hoặc 300 Ringgit tại UNIQLO &amp; Outlet.</td>
            </tr>
            <tr>
                <td></td>
                <td>BBKLCC Tourist Exclusive</td>
                <td>Quà tặng túi mua sắm với mỗi hóa đơn trị giá 100 Ringgit hoặc 300 Ringgit tại UNIQLO.</td>
            </tr>
        </table>
    </div>
</asp:Content>
