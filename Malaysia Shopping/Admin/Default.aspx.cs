﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Malaysia_Shopping.Admin
{
    public partial class Default : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            AdminViews.SetActiveView(LoginView);

            var cookie = Request.Cookies[FormsAuthentication.FormsCookieName];
            if (cookie != null && !string.IsNullOrEmpty(cookie.Value))
            {
                var formTicket = FormsAuthentication.Decrypt(cookie.Value);
                if (!formTicket.Expired && formTicket.Name.ToLower() == "admin")
                {
                    AdminViews.SetActiveView(DefaultView);

                    var entities = new Malaysia_Shopping_AppEntities();
                    TotalUsers.Text = entities.Users.Count().ToString();
                    PlayCount.Text = entities.Leaderboards.Count().ToString();
                    CameraCount.Text = entities.Leaderboards.Count(l => l.Prize == 0).ToString();
                    BagCount.Text = entities.Leaderboards.Count(l => l.Prize == 1).ToString();
                    PassportCount.Text = entities.Leaderboards.Count(l => l.Prize == 2).ToString();
                    OtherCount.Text = entities.Leaderboards.Count(l => l.Prize == 3).ToString();
                }
            }
        }

        protected void Login_Authenticate(object sender, AuthenticateEventArgs e)
        {
            if (Login.UserName.ToLower() == "admin" && Login.Password == "malaysia123#@!")
            {
                FormsAuthentication.SetAuthCookie("Admin", true);
                Response.Redirect("Default.aspx");
            }
        }

        protected void ExportToFile_Click(object sender, EventArgs e)
        {
            var entities = new Malaysia_Shopping_AppEntities();

            var data = from u in entities.Users
                       select new
                                  {
                                      u.FacebookID,
                                      u.FacebookName,
                                      u.FullName,
                                      u.PhoneNumber,
                                      u.Address,
                                      u.Email,
                                      u.CreatedOn,
                                      LastPlay = u.Leaderboards.Max(l => l.CreatedOn),
                                      Prize = u.Leaderboards.Max(l => l.Prize)
                                  };

            var result = new StringBuilder();

            result.AppendLine("<table border='1'>");
            result.AppendLine("<tr>");
            result.AppendLine("<thead style='font-weight: bold;'>");
            result.AppendLine("<td>Facebook ID</td>");
            result.AppendLine("<td>Facebook Name</td>");
            result.AppendLine("<td>Fullname</td>");
            result.AppendLine("<td>Phone number</td>");
            result.AppendLine("<td>Address</td>");
            result.AppendLine("<td>Email</td>");
            result.AppendLine("<td>Created On</td>");
            result.AppendLine("<td>Last play</td>");
            result.AppendLine("<td>Prize</td>");
            result.AppendLine("</thead>");
            result.AppendLine("</tr>");

            string[] PrizeNames = new string[]
                                            {
                                                "Fujifilm Instant Camera dòng mới cực cool",
                                                "Túi du lịch mini",
                                                "Bao đựng passport",
                                                "Phần quà hấp dẫn khác"
                                            };

            foreach (var item in data)
            {
                result.AppendLine("<tr>");
                result.AppendFormat("<td>'{0}</td>", item.FacebookID);
                result.AppendFormat("<td>{0}</td>", item.FacebookName);
                result.AppendFormat("<td>{0}</td>", item.FullName);
                result.AppendFormat("<td>'{0}</td>", item.PhoneNumber);
                result.AppendFormat("<td>{0}</td>", item.Address);
                result.AppendFormat("<td>{0}</td>", item.Email);
                result.AppendFormat("<td>{0}</td>", item.CreatedOn);
                result.AppendFormat("<td>{0}</td>", item.LastPlay);
                result.AppendFormat("<td>{0}</td>", item.Prize == -1 ? "Không trúng giải" : PrizeNames[item.Prize]);
                result.AppendLine("</tr>");
            }

            result.AppendLine("</table>");

            Response.Clear();
            Response.ContentType = "application/ms-excel;charset=utf-8";
            Response.AddHeader("Content-Disposition", "attachment; filename=LuckyShopping.xls");
            Response.BinaryWrite(System.Text.Encoding.UTF8.GetBytes(result.ToString()));
            Response.End();
        }

        protected void FilterButton_Click(object sender, EventArgs e)
        {
            RadGrid1.Rebind();
        }
        
        protected void ClearFilterButton_Click(object sender, EventArgs e)
        {
            FromDate.Clear();
            ToDate.Clear();
            RadGrid1.Rebind();
        }
    }
}