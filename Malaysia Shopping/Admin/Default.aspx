﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Default.aspx.cs" Inherits="Malaysia_Shopping.Admin.Default" %>

<%@ Import Namespace="Malaysia_Shopping" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
        <div style="font-family: Arial;">
            <asp:MultiView runat="server" ID="AdminViews" ActiveViewIndex="0">
                <asp:View runat="server" ID="LoginView">
                    <asp:Login runat="server" UserName="Admin" ID="Login" OnAuthenticate="Login_Authenticate" Style="margin: auto; font-family: Arial;">
                    </asp:Login>
                </asp:View>
                <asp:View runat="server" ID="DefaultView">
                    <telerik:RadAjaxManager ID="RadAjaxManager1" runat="server">
                        <AjaxSettings>
                            <telerik:AjaxSetting AjaxControlID="RadGrid1">
                                <UpdatedControls>
                                    <telerik:AjaxUpdatedControl ControlID="RadGrid1" LoadingPanelID="LoadingPanel1"></telerik:AjaxUpdatedControl>
                                </UpdatedControls>
                            </telerik:AjaxSetting>
                        </AjaxSettings>
                    </telerik:RadAjaxManager>
                    <telerik:RadScriptManager ID="RadScriptManager1" runat="server">
                        <Scripts>
                            <asp:ScriptReference Assembly="Telerik.Web.UI" Name="Telerik.Web.UI.Common.Core.js"></asp:ScriptReference>
                            <asp:ScriptReference Assembly="Telerik.Web.UI" Name="Telerik.Web.UI.Common.jQuery.js"></asp:ScriptReference>
                            <asp:ScriptReference Assembly="Telerik.Web.UI" Name="Telerik.Web.UI.Common.jQueryInclude.js"></asp:ScriptReference>
                        </Scripts>
                    </telerik:RadScriptManager>
                    <telerik:RadAjaxLoadingPanel runat="server" ID="LoadingPanel1"></telerik:RadAjaxLoadingPanel>
                    <asp:Panel ID="RadPane1" runat="server" BorderStyle="None" Font-Names="Arial" GroupingText="Thống kê">
                        Có
                        <strong>
                            <asp:Literal runat="server" ID="TotalUsers">0</asp:Literal></strong>
                        người tham gia, <strong>
                            <asp:Literal runat="server" ID="PlayCount">0</asp:Literal></strong> lượt chơi
                        <br />
                        Có <strong>
                            <asp:Literal runat="server" ID="CameraCount">0</asp:Literal></strong> người trúng máy ảnh Fujifilm, <strong>
                                <asp:Literal runat="server" ID="BagCount">0</asp:Literal></strong> người trúng túi xách, <strong>
                                    <asp:Literal runat="server" ID="PassportCount">0</asp:Literal></strong> người trúng bao passport, <strong>
                                        <asp:Literal runat="server" ID="OtherCount">0</asp:Literal></strong> người trúng giải thưởng khác.
                    </asp:Panel>
                    <br />
                    <label>Filter by "Last play date"</label>
                    <label>from</label>
                    <telerik:RadDatePicker runat="server" ID="FromDate"></telerik:RadDatePicker>
                    <label>to</label>
                    <telerik:RadDatePicker runat="server" ID="ToDate"></telerik:RadDatePicker>
                    <telerik:RadButton runat="server" ID="FilterButton" Text="Filter" OnClick="FilterButton_Click"></telerik:RadButton>
                    &nbsp;&nbsp;
                    <telerik:RadButton runat="server" ID="ClearFilter" Text="Clear Filter" OnClick="ClearFilterButton_Click"></telerik:RadButton>
                    &nbsp;&nbsp;&nbsp;&nbsp;
                    <telerik:RadButton runat="server" ID="ExportToFile" Text="Export to Excel" OnClick="ExportToFile_Click"></telerik:RadButton>
                    <br />
                    <br />
                    <telerik:RadGrid ID="RadGrid1" runat="server" AllowFilteringByColumn="True" AllowPaging="True" AllowSorting="True" CellSpacing="0" DataSourceID="SqlDataSource1" GridLines="None" ShowFooter="True" Skin="WebBlue" Width="100%" PageSize="50">
                        <MasterTableView AutoGenerateColumns="False" DataKeyNames="ID" DataSourceID="SqlDataSource1">
                            <PagerStyle PageSizes="20,50,100,200"></PagerStyle>
                            <CommandItemSettings ExportToPdfText="Export to PDF" />
                            <RowIndicatorColumn FilterControlAltText="Filter RowIndicator column" Visible="True">
                                <HeaderStyle Width="20px" />
                            </RowIndicatorColumn>
                            <ExpandCollapseColumn FilterControlAltText="Filter ExpandColumn column" Visible="True">
                                <HeaderStyle Width="20px" />
                            </ExpandCollapseColumn>
                            <DetailTables>
                                <telerik:GridTableView runat="server" DataSourceID="EntityDataSource2" DataKeyNames="ID" AutoGenerateColumns="False" Width="100%">
                                    <ParentTableRelation>
                                        <telerik:GridRelationFields DetailKeyField="UserID" MasterKeyField="ID" />
                                    </ParentTableRelation>
                                    <CommandItemSettings ExportToPdfText="Export to PDF" />
                                    <RowIndicatorColumn FilterControlAltText="Filter RowIndicator column" Visible="True">
                                    </RowIndicatorColumn>
                                    <ExpandCollapseColumn FilterControlAltText="Filter ExpandColumn column" Visible="True">
                                    </ExpandCollapseColumn>
                                    <Columns>
                                        <telerik:GridBoundColumn CurrentFilterFunction="GreaterThanOrEqualTo" DataField="CreatedOn" DataType="System.DateTime" FilterControlAltText="Filter CreatedOn column" HeaderText="Created On" SortExpression="CreatedOn" UniqueName="CreatedOn" AllowFiltering="False">
                                        </telerik:GridBoundColumn>
                                        <telerik:GridTemplateColumn AutoPostBackOnFilter="True" CurrentFilterFunction="Contains" DataField="Prize" FilterControlAltText="Filter Prize column" HeaderText="Prize" SortExpression="Prize" UniqueName="Prize" AllowFiltering="False">
                                            <ItemTemplate>
                                                <%#
                                      (int)Eval("Prize") == - 1 ? "Không trúng giải" :
                                                                                         new string[]
                                                                                             {
                                                                                                 "Fujifilm Instant Camera dòng mới cực cool",
                                                                                                 "Túi du lịch mini",
                                                                                                 "Bao đựng passport",
                                                                                                 "Phần quà hấp dẫn khác"
                                                                                             }[(int)Eval("Prize")]
                                                %>
                                            </ItemTemplate>
                                        </telerik:GridTemplateColumn>
                                        <telerik:GridBoundColumn DataField="IP" DataType="System.String" FilterControlAltText="Filter IP column" HeaderText="IP" ReadOnly="True" SortExpression="IP" UniqueName="IP" AllowFiltering="False">
                                        </telerik:GridBoundColumn>
                                    </Columns>
                                    <EditFormSettings>
                                        <EditColumn FilterControlAltText="Filter EditCommandColumn column">
                                        </EditColumn>
                                    </EditFormSettings>
                                </telerik:GridTableView>
                            </DetailTables>
                            <Columns>
                                <telerik:GridBoundColumn DataField="ID" DataType="System.Int32" FilterControlAltText="Filter ID column" HeaderText="ID" ReadOnly="True" SortExpression="ID" UniqueName="ID" Visible="False">
                                </telerik:GridBoundColumn>
                                <telerik:GridBoundColumn DataField="FacebookID" FilterControlAltText="Filter FacebookID column" HeaderText="FacebookID" SortExpression="FacebookID" UniqueName="FacebookID" AutoPostBackOnFilter="True" CurrentFilterFunction="Contains">
                                </telerik:GridBoundColumn>
                                <telerik:GridBoundColumn DataField="FacebookName" FilterControlAltText="Filter FacebookName column" HeaderText="FacebookName" SortExpression="FacebookName" UniqueName="FacebookName" AutoPostBackOnFilter="True" CurrentFilterFunction="Contains">
                                </telerik:GridBoundColumn>
                                <telerik:GridBoundColumn CurrentFilterFunction="GreaterThanOrEqualTo" DataField="CreatedOn" DataType="System.DateTime" FilterControlAltText="Filter CreatedOn column" HeaderText="Created On" SortExpression="CreatedOn" UniqueName="CreatedOn">
                                </telerik:GridBoundColumn>
                                <telerik:GridBoundColumn DataField="FullName" FilterControlAltText="Filter FullName column" HeaderText="FullName" SortExpression="FullName" UniqueName="FullName" AutoPostBackOnFilter="True" CurrentFilterFunction="Contains">
                                </telerik:GridBoundColumn>
                                <telerik:GridBoundColumn DataField="IDNumber" FilterControlAltText="Filter IDNumber column" HeaderText="IDNumber" SortExpression="IDNumber" UniqueName="IDNumber" Visible="False">
                                </telerik:GridBoundColumn>
                                <telerik:GridBoundColumn DataField="PhoneNumber" FilterControlAltText="Filter PhoneNumber column" HeaderText="PhoneNumber" SortExpression="PhoneNumber" UniqueName="PhoneNumber" AutoPostBackOnFilter="True" CurrentFilterFunction="Contains">
                                </telerik:GridBoundColumn>
                                <telerik:GridBoundColumn DataField="Address" FilterControlAltText="Filter Address column" HeaderText="Address" SortExpression="Address" UniqueName="Address" AutoPostBackOnFilter="True" CurrentFilterFunction="Contains">
                                </telerik:GridBoundColumn>
                                <telerik:GridBoundColumn DataField="Email" FilterControlAltText="Filter Email column" HeaderText="Email" SortExpression="Email" UniqueName="Email" AutoPostBackOnFilter="True" CurrentFilterFunction="Contains">
                                </telerik:GridBoundColumn>
                                <telerik:GridBoundColumn CurrentFilterFunction="GreaterThanOrEqualTo" DataField="PlayOn" DataType="System.DateTime" FilterControlAltText="Filter PlayOn column" HeaderText="Last play On" SortExpression="PlayOn" UniqueName="PlayOn" AutoPostBackOnFilter="True">
                                </telerik:GridBoundColumn>
                                <telerik:GridTemplateColumn AutoPostBackOnFilter="True" CurrentFilterFunction="Contains" DataField="Prize" FilterControlAltText="Filter Prize column" HeaderText="Prize" SortExpression="Prize" UniqueName="Prize" AllowFiltering="False">
                                    <ItemTemplate>
                                        <%#
                                      (int)Eval("Prize") == - 1 ? "Không trúng giải" :
                                                                                         new string[]
                                                                                             {
                                                                                                 "Fujifilm Instant Camera dòng mới cực cool",
                                                                                                 "Túi du lịch mini",
                                                                                                 "Bao đựng passport",
                                                                                                 "Phần quà hấp dẫn khác"
                                                                 }[(int)Eval("Prize")]
                                        %>
                                    </ItemTemplate>
                                </telerik:GridTemplateColumn>
                            </Columns>
                            <EditFormSettings>
                                <EditColumn FilterControlAltText="Filter EditCommandColumn column">
                                </EditColumn>
                            </EditFormSettings>
                        </MasterTableView>
                        <FilterMenu EnableImageSprites="False">
                        </FilterMenu>
                        <GroupingSettings CaseSensitive="False"></GroupingSettings>
                    </telerik:RadGrid>
                    <asp:SqlDataSource ID="SqlDataSource1" runat="server" ConnectionString="<%$ ConnectionStrings:DefaultConnection %>" SelectCommand="SELECT Board.UserID, Board.PlayOn, Board.Prize, [User].ID, [User].FacebookID, [User].FacebookName, [User].CreatedOn, [User].FullName, [User].IDNumber, [User].PhoneNumber, [User].Address, [User].Email FROM (SELECT UserID, MAX(CreatedOn) AS PlayOn, MAX(Prize) AS Prize FROM Leaderboard GROUP BY UserID) AS Board LEFT OUTER JOIN [User] ON Board.UserID = [User].ID WHERE (Board.PlayOn &gt;= @FromDate) AND (Board.PlayOn &lt; @EndDate)">
                        <SelectParameters>
                            <asp:ControlParameter ControlID="FromDate" DefaultValue="2012-12-10" Name="FromDate" PropertyName="SelectedDate" />
                            <asp:ControlParameter ControlID="ToDate" DefaultValue="2013-01-05" Name="EndDate" PropertyName="SelectedDate" />
                        </SelectParameters>
                    </asp:SqlDataSource>
                    <asp:EntityDataSource ID="EntityDataSource2" runat="server" ConnectionString="name=Malaysia_Shopping_AppEntities" DefaultContainerName="Malaysia_Shopping_AppEntities" EnableFlattening="False" EntitySetName="Leaderboards" AutoGenerateWhereClause="True">
                        <WhereParameters>
                            <asp:SessionParameter DefaultValue="0" Name="UserID" SessionField="UserID" Type="Int32" />
                        </WhereParameters>
                    </asp:EntityDataSource>
                </asp:View>
            </asp:MultiView>
        </div>
    </form>
</body>
</html>
