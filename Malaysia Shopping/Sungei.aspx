﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Popup.Master" AutoEventWireup="true" CodeBehind="Sungei.aspx.cs" Inherits="Malaysia_Shopping.Sungei" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <style type="text/css">
        html, body {
            padding: 20px 10px 40px 20px;
        }

        .content {
            width: 526px;
            height: 500px;
            padding-right: 30px;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="content">
        <table cellpadding="0" cellspacing="0">
            <thead>
                <tr>
                    <td>Ngày</td>
                    <td>Sự kiện/ Khuyến mãi</td>
                    <td>Chi tiết</td>
                </tr>
            </thead>
            <tr>
                <td>10/11-1/1/13</td>
                <td>Shop & Win Yoga Lin Concert Tickets</td>
                <td>Cùng tận hưởng buổi mua sắm không ngừng nghỉ tại Sungei Wang Plaza trong dịp MYES (mùa khuyến mãi mua sắm cuối năm)! Chỉ với RM35 cho mỗi hóa đơn ở bất kì gian hàng nào quý khách đều có cơ hội sở hữu 1 cặp vé hòa nhạc Yoga Lin. Tổng cộng sẽ có 10 cặp vé xem hòa nhạc sẽ được phát đi.
                </td>
            </tr>
            <tr>
                <td>29/11–25/12</td>
                <td>The Big Band Christmas</td>
                <td>Ban nhạc chơi bộ gõ tài năng đi kèm với những vũ công và cờ Giáng Sinh sẽ giúp bạn tận hưởng mùa Giáng sinh năm nay. Buổi diễn này sẽ đem đến những bất ngờ cho  khán giả. Hãy cùng nhau chờ đón mùa Giáng Sinh này.
                </td>
            </tr>
            <tr>
                <td>31/12</td>
                <td>Street Party Countdown 2013</td>
                <td>Lễ hội đường phố sẽ được tổ chức trước trung tâm Sungei Wang đi kèm với phần trình diễn của những nghệ sĩ Trung Quốc; sau đó sẽ là màn trình diễn pháo hoa đặc sắc ngay khi bước qua ngày mới.
                </td>
            </tr>
        </table>
    </div>
</asp:Content>
