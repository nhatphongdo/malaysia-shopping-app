﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Malaysia_Shopping
{
    public partial class Play : System.Web.UI.Page
    {
        protected string[] PrizeNames = new string[]
                                            {
                                                "Fujifilm Instant Camera dòng mới cực cool",
                                                "Túi du lịch mini",
                                                "Bao đựng passport",
                                                "Phần quà hấp dẫn khác"
                                            };

        protected string[] PrizeImages = new string[]
                                             {
                                                 "Images/prize-camera.png",
                                                 "Images/prize-bag.png",
                                                 "Images/prize-passportcover.png",
                                                 "Images/prize-other.png"
                                             };

        private static readonly Random _random = new Random((int)DateTime.Now.Ticks);

        protected DateTime _campaignStart = new DateTime(2012, 12, 10);
        protected DateTime _campaignEnd = new DateTime(2013, 1, 5);

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                if (!string.IsNullOrEmpty(Request["fid"]))
                {
                    using (var entities = new Malaysia_Shopping_AppEntities())
                    {
                        var startDay = new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day, 0, 0, 0, 0);
                        var endDay = new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day, 23, 59, 59, 999);

                        var facebookID = Request["fid"];
                        var user = entities.Users.FirstOrDefault(u => u.FacebookID == facebookID);
                        if (user == null)
                        {
                            //Views.SetActiveView(PlayView);
                            return;
                        }

                        var leaderboard = entities.Leaderboards.FirstOrDefault(l => l.UserID == user.ID && l.CreatedOn >= startDay && l.CreatedOn <= endDay);

                        if (leaderboard == null)
                        {
                            // Not play yet
                            //Views.SetActiveView(PlayView);
                        }
                        else
                        {
                            // Played
                            //PlayedShareLink.Text = string.Format(
                            //    "<a class='button share' style='margin-left: 183px; margin-top: 10px;' image_link='{0}'>Chia sẻ</a>",
                            //    leaderboard.Prize == -1 ? "Images/logo.png" : PrizeImages[leaderboard.Prize]);

                            Views.SetActiveView(PlayedView);
                        }
                    }
                }
                else
                {
                    //Views.SetActiveView(PlayView);
                }
            }
        }

        protected void Good_Click(object sender, EventArgs e)
        {
            var facebookID = FacebookID.Value;
            if (string.IsNullOrEmpty(facebookID))
            {
                return;
            }

            using (var entities = new Malaysia_Shopping_AppEntities())
            {
                // Check user existence
                var user = entities.Users.FirstOrDefault(u => u.FacebookID == facebookID);
                if (user == null)
                {
                    // Create user
                    user = new User()
                               {
                                   CreatedOn = DateTime.Now,
                                   FacebookID = facebookID,
                                   FacebookName = FacebookName.Value
                               };

                    entities.Users.Add(user);
                    entities.SaveChanges();
                }

                // Check leaderboard
                var startDay = new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day, 0, 0, 0, 0);
                var endDay = new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day, 23, 59, 59, 999);
                var leaderboard = entities.Leaderboards.FirstOrDefault(l => l.UserID == user.ID && l.CreatedOn >= startDay && l.CreatedOn <= endDay);

                if (leaderboard != null)
                {
                    // Already played
                    //PlayedShareLink.Text = string.Format(
                    //        "<a class='button share' style='margin-left: 183px; margin-top: 10px;' image_link='{0}'>Chia sẻ</a>",
                    //        leaderboard.Prize == -1 ? "Images/logo.png" : PrizeImages[leaderboard.Prize]);
                    Views.SetActiveView(PlayedView);
                    return;
                }

                // Get the number of prizes

                // Get already won status
                var alreadyWon = entities.Leaderboards.Any(l => l.UserID == user.ID && l.Prize >= 0);

                // Generate prize
                leaderboard = new Leaderboard()
                                  {
                                      CreatedOn = DateTime.Now,
                                      Prize = alreadyWon ? -1 : GeneratePrize(facebookID),
                                      UserID = user.ID,
                                      IP = Utility.GetIPAddress()
                                  };

                entities.Leaderboards.Add(leaderboard);
                entities.SaveChanges();

                if (leaderboard.Prize == -1)
                {
                    // Lose
                    FailShareLink.Text = "<a class='share' style='display: none;' image_link='Images/logo.png'>Chia sẻ</a>";
                    Views.SetActiveView(FailView);
                }
                else
                {
                    // Win
                    GiftName.Text = PrizeNames[leaderboard.Prize];
                    GiftImage.ImageUrl = PrizeImages[leaderboard.Prize];
                    Views.SetActiveView(WinView);
                }
            }
        }

        protected int GeneratePrize(string facebookID)
        {
            if (DateTime.Now < _campaignStart || DateTime.Now >= _campaignEnd || string.IsNullOrEmpty(facebookID))
            {
                return -1;
            }

            // 5 prize 0: Fujifilm camera
            // 40 prize 1: Bag
            // 70 prize 2: Passport cover
            // 150 prize 3: Others
            var week2 = _campaignStart.AddDays(7);
            var week3 = _campaignStart.AddDays(14);
            var week4 = _campaignStart.AddDays(21);

            var startWeek = DateTime.Now;
            if (startWeek > week4)
            {
                startWeek = week4;
            }
            else if (startWeek > week3)
            {
                startWeek = week3;
            }
            else if (startWeek > week2)
            {
                startWeek = week2;
            }
            else if (startWeek > _campaignStart)
            {
                startWeek = _campaignStart;
            }

            var endWeek = startWeek.AddDays(7);
            if (endWeek > _campaignEnd)
            {
                endWeek = _campaignEnd;
            }

            using (var entities = new Malaysia_Shopping_AppEntities())
            {
                var rand = _random.Next(10000);
                if (rand % 150 == 0)
                {
                    var cameraCount =
                        entities.Leaderboards.Count(
                            l => l.Prize == 0 && l.CreatedOn >= startWeek && l.CreatedOn < endWeek);
                    var bagCount =
                        entities.Leaderboards.Count(
                            l => l.Prize == 1 && l.CreatedOn >= startWeek && l.CreatedOn < endWeek);
                    var coverCount =
                        entities.Leaderboards.Count(
                            l => l.Prize == 2 && l.CreatedOn >= startWeek && l.CreatedOn < endWeek);
                    var otherCount =
                        entities.Leaderboards.Count(
                            l => l.Prize == 3 && l.CreatedOn >= startWeek && l.CreatedOn < endWeek);

                    if (entities.Leaderboards.Count(l => l.Prize == 0) >= 5)
                    {
                        cameraCount = int.MaxValue;
                    }
                    if (entities.Leaderboards.Count(l => l.Prize == 1) >= 40)
                    {
                        bagCount = int.MaxValue;
                    }
                    if (entities.Leaderboards.Count(l => l.Prize == 2) >= 70)
                    {
                        coverCount = int.MaxValue;
                    }
                    if (entities.Leaderboards.Count(l => l.Prize == 3) >= 150)
                    {
                        otherCount = int.MaxValue;
                    }

                    var prizeList = new List<int>();
                    for (var i = 0; i < 1 - cameraCount; i++)
                    {
                        prizeList.Add(0);
                    }
                    for (var i = 0; i < 10 - bagCount; i++)
                    {
                        prizeList.Add(1);
                    }
                    for (var i = 0; i < 17 - coverCount; i++)
                    {
                        prizeList.Add(2);
                    }
                    for (var i = 0; i < 37 - otherCount; i++)
                    {
                        prizeList.Add(3);
                    }

                    if (prizeList.Count == 0)
                    {
                        return -1;
                    }

                    var prizeIndex = _random.Next(0, prizeList.Count);
                    return prizeList[prizeIndex];
                }
                else
                {
                    return -1;
                }
            }
        }

        protected void Submit_Click(object sender, EventArgs e)
        {
            using (var entities = new Malaysia_Shopping_AppEntities())
            {
                // Validate
                ErrorMessage.Text = string.Empty;

                if (string.IsNullOrEmpty(Fullname.Text))
                {
                    ErrorMessage.Text = "Bạn chưa nhập Họ và Tên";
                    return;
                }

                if (string.IsNullOrEmpty(Email.Text))
                {
                    ErrorMessage.Text = "Bạn chưa nhập Địa chỉ Email";
                    return;
                }

                var regexEmail =
                    new System.Text.RegularExpressions.Regex(
                        @"[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?");
                if (!regexEmail.IsMatch(Email.Text))
                {
                    ErrorMessage.Text = "Địa chỉ Email không hợp lệ";
                    return;
                }

                var oldUser = entities.Users.FirstOrDefault(u => u.Email.ToLower() == Email.Text.ToLower());
                if (oldUser != null)
                {
                    ErrorMessage.Text = "Địa chỉ Email này đã được sử dụng";
                    return;
                }

                if (string.IsNullOrEmpty(PhoneNumber.Text))
                {
                    ErrorMessage.Text = "Bạn chưa nhập Số điện thoại";
                    return;
                }

                var regexPhoneNumber = new System.Text.RegularExpressions.Regex(@"^[0-9]+$");
                if (!regexPhoneNumber.IsMatch(PhoneNumber.Text) || PhoneNumber.Text.Length < 6 || PhoneNumber.Text.Length > 15)
                {
                    ErrorMessage.Text = "Số điện thoại không hợp lệ";
                    return;
                }

                if (string.IsNullOrEmpty(Fullname.Text))
                {
                    ErrorMessage.Text = "Bạn chưa nhập Địa chỉ";
                    return;
                }

                var user = entities.Users.FirstOrDefault(u => u.FacebookID == FacebookID.Value);
                if (user == null)
                {
                    ErrorMessage.Text = "Có lỗi xảy ra trong quá trình xử lý. Bạn hãy chờ một lát và thử lại.";
                    return;
                }

                user.Email = Email.Text;
                user.FullName = Fullname.Text;
                user.Address = Address.Text;
                user.PhoneNumber = PhoneNumber.Text;

                entities.SaveChanges();

                var leaderboard = entities.Leaderboards.Where(l => l.UserID == user.ID).OrderByDescending(l => l.CreatedOn).FirstOrDefault();

                if (leaderboard != null)
                {
                    ShareLink.Text = string.Format(
                            "<a class='share' style='display: none;' image_link='{0}'>Chia sẻ</a>",
                            leaderboard.Prize == -1 ? "Images/logo.png" : PrizeImages[leaderboard.Prize]);
                }

                Views.SetActiveView(ShareView);
            }
        }
    }
}