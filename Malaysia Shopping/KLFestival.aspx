﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Popup.Master" AutoEventWireup="true" CodeBehind="KLFestival.aspx.cs" Inherits="Malaysia_Shopping.KLFestival" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <style type="text/css">
        html, body {
            padding: 20px 10px 40px 20px;
        }

        .content {
            width: 526px;
            height: 500px;
            padding-right: 30px;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="content">
        <table cellpadding="0" cellspacing="0">
            <thead>
                <tr>
                    <td>Ngày</td>
                    <td>Sự kiện/ Khuyến mãi</td>
                    <td>Chi tiết</td>
                </tr>
            </thead>
            <tr>
                <td>26/11 onwards</td>
                <td>Christmas 2012</td>
                <td>Quà tặng là một đôi thiệp thiết kế theo phiên bản đặc biệt mừng Giáng sinh cho hóa đơn  từ 300 Ringgit trở lên tại bất kỳ trung tâm mua sắm nào (gộp tối đa 2 biên lai). Số lượng quà có hạn.</td>
            </tr>
        </table>
    </div>
</asp:Content>
