﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="ShoppingCenters.aspx.cs" Inherits="Malaysia_Shopping.ShoppingCenters" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <webopt:BundleReference runat="server" Path="~/Content/css" />
    <link href="~/favicon.ico" rel="shortcut icon" type="image/x-icon" />
    <meta name="viewport" content="width=device-width" />
    <script src="Scripts/jquery-1.7.1.min.js"></script>
    <script src="Scripts/site.js"></script>

    <script type="text/javascript">

        var _gaq = _gaq || [];
        _gaq.push(['_setAccount', 'UA-3013465-44']);
        _gaq.push(['_trackPageview']);

        (function () {
            var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
            ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
            var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
        })();

    </script>
</head>
<body>
    <div id="fb-root"></div>

    <script type="text/javascript">
        window.fbAsyncInit = function () {
            FB.init({
                appId: '571497422866897', // App ID
                channelUrl: '//www.vietdev.vn/malaysia_shopping_app/channel.html', // Channel File
                status: true, // check login status
                cookie: true, // enable cookies to allow the server to access the session
                xfbml: true  // parse XFBML
            });

            FB.Canvas.setAutoGrow();
        };

        // Load the SDK Asynchronously
        (function (d) {
            var js, id = 'facebook-jssdk', ref = d.getElementsByTagName('script')[0];
            if (d.getElementById(id)) { return; }
            js = d.createElement('script'); js.id = id; js.async = true;
            js.src = "//connect.facebook.net/en_US/all.js";
            ref.parentNode.insertBefore(js, ref);
        }(document));
    </script>
    
    <form id="form1" runat="server">
        <div class="page shopping-centers">
            <ul class="center-list">
                <li>
                    <a href="javascript:void(0);" onclick="showPopup('Sungei Wang Plaza', 616, 613, 'Sungei.aspx', 'main-popup');">
                        <img src="Images/center01.jpg" alt="Sungei Wang Plaza" />
                    </a>
                    <a class="title" href="javascript:void(0);" onclick="showPopup('Sungei Wang Plaza', 616, 613, 'Sungei.aspx', 'main-popup');">Sungei Wang Plaza</a>
                </li>
                <li>
                    <a href="javascript:void(0);" onclick="showPopup('Central Market', 616, 613, 'CentralMarket.aspx', 'main-popup');">
                        <img src="Images/center02.jpg" alt="Central Market" />
                    </a>
                    <a class="title" href="javascript:void(0);" onclick="showPopup('Central Market', 616, 613, 'CentralMarket.aspx', 'main-popup');">Central Market</a>
                </li>
                <li>
                    <a href="javascript:void(0);" onclick="showPopup('Kenanga Wholesale City', 616, 613, 'Kenanga.aspx', 'main-popup');">
                        <img src="Images/center03.jpg" alt="Kenanga Wholesale City" />
                    </a>
                    <a class="title" href="javascript:void(0);" onclick="showPopup('Kenanga Wholesale City', 616, 613, 'Kenanga.aspx', 'main-popup');">Kenanga Wholesale City</a>
                </li>
                <li>
                    <a href="javascript:void(0);" onclick="showPopup('Bangsar Shopping Centre', 616, 613, 'Bangsar.aspx', 'main-popup');">
                        <img src="Images/center04.jpg" alt="Bangsar Shopping Centre" />
                    </a>
                    <a class="title" href="javascript:void(0);" onclick="showPopup('Bangsar Shopping Centre', 616, 613, 'Bangsar.aspx', 'main-popup');">Bangsar Shopping Centre</a>
                </li>
                <li>
                    <a href="javascript:void(0);" onclick="showPopup('Berjaya Times Square', 616, 613, 'Berjaya.aspx', 'main-popup');">
                        <img src="Images/center05.jpg" alt="Berjaya Times Square" />
                    </a>
                    <a class="title" href="javascript:void(0);" onclick="showPopup('Berjaya Times Square', 616, 613, 'Berjaya.aspx', 'main-popup');">Berjaya Times Square</a>
                </li>
                <li>
                    <a href="javascript:void(0);" onclick="showPopup('Fahrenheit88', 616, 613, 'Fahrenheit.aspx', 'main-popup');">
                        <img src="Images/center06.jpg" alt="Fahrenheit88" />
                    </a>
                    <a class="title" href="javascript:void(0);" onclick="showPopup('Fahrenheit88', 616, 613, 'Fahrenheit.aspx', 'main-popup');">Fahrenheit88</a>
                </li>
                <li>
                    <a href="javascript:void(0);" onclick="showPopup('KL Festival City', 616, 613, 'KLFestival.aspx', 'main-popup');">
                        <img src="Images/center07.jpg" alt="KL Festival City" />
                    </a>
                    <a class="title" href="javascript:void(0);" onclick="showPopup('KL Festival City', 616, 613, 'KLFestival.aspx', 'main-popup');">KL Festival City</a>
                </li>
                <li>
                    <a href="javascript:void(0);" onclick="showPopup('Suria KLCC', 616, 613, 'Suria.aspx', 'main-popup');">
                        <img src="Images/center08.jpg" alt="Suria KLCC" />
                    </a>
                    <a class="title" href="javascript:void(0);" onclick="showPopup('Suria KLCC', 616, 613, 'Suria.aspx', 'main-popup');">Suria KLCC</a>
                </li>
                <li>
                    <a href="javascript:void(0);" onclick="showPopup('Pavilion KL', 616, 613, 'Pavilion.aspx', 'main-popup');">
                        <img src="Images/center09.jpg" alt="Pavilion KL" />
                    </a>
                    <a class="title" href="javascript:void(0);" onclick="showPopup('Pavilion KL', 616, 613, 'Pavilion.aspx', 'main-popup');">Pavilion KL</a>
                </li>
                <li>
                    <a href="javascript:void(0);" onclick="showPopup('Kenanga Wholesale City', 616, 613, 'Kenanga.aspx', 'main-popup');">
                        <img src="Images/center10.jpg" alt="Kenanga Wholesale City" />
                    </a>
                    <a class="title" href="javascript:void(0);" onclick="showPopup('Kenanga Wholesale City', 616, 613, 'Kenanga.aspx', 'main-popup');">Kenanga Wholesale City</a>
                </li>
            </ul>
            <div id="main-popup" class="popup-bg">
                <div class="title">
                    <span></span>
                    <a class="close" href="javascript:void(0);" onclick="hidePopup('main-popup');"></a>
                </div>
                <div class="body"></div>
            </div>
        </div>
    </form>
</body>
</html>
